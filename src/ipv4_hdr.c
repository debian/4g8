/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "ipv4_hdr.h"

void
ipv4_hdr(struct libnet_ipv4_hdr *iphdr, u_int16_t prv_opts)
{
    u_int8_t *s_addr, *d_addr;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: ipv4_hdr()\n");
#endif

    s_addr = malloc(sizeof(u_int8_t *));
    d_addr = malloc(sizeof(u_int8_t *));

    s_addr = libnet_addr2name4(iphdr->ip_src.s_addr, 0);
    d_addr = libnet_addr2name4(iphdr->ip_dst.s_addr, 0);

    fprintf(stdout, "IP header:   Src Address: %s  Dst Address: %s\n", s_addr, d_addr); 

    fprintf(stdout, "\t     TTL: %d  ID: %d  TOS: 0x%X  Len: %d  ", 
        iphdr->ip_ttl, 
	ntohs(iphdr->ip_id), 
	iphdr->ip_tos, 
	ntohs(iphdr->ip_len));

    if(ntohs(iphdr->ip_off) & IP_DF)
        fprintf(stdout, "(DF)  ");

    fprintf(stdout, "\n");

    if(libnet_build_ipv4(
        ntohs(iphdr->ip_len) - prv_opts,
        iphdr->ip_tos,
        ntohs(iphdr->ip_id),
        ntohs(iphdr->ip_off),
        iphdr->ip_ttl,
        iphdr->ip_p,
        0,
        iphdr->ip_src.s_addr,
        iphdr->ip_dst.s_addr,
        NULL,
        0,
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build IP header: %s", libnet_geterror(pkt_d));
    }

    return;
}
