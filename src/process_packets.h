/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#ifndef __PROCESS_PACKETS_H
#define __PROCESS_PACKETS_H

#include "globals.h"
#include "ethernet_hdr.h"
#include "icmpv4_hdr.h"
#include "ipv4_hdr.h"
#include "tcp_hdr.h"
#include "udp_hdr.h"
#include "dump_packet.h"
#include "arp_cache.h"

void process_packets(struct pcap_pkthdr *, u_int8_t *);
u_int16_t inject_packet();

#endif /* __PROCESS_PACKETS_H */
