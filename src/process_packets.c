/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "process_packets.h"

u_int16_t
inject_packet()
{
#ifdef DEBUG
    fprintf(stdout, "DEBUG: inject_packet()\n");
#endif

    if(libnet_write(pkt_d) == -1)
        return FAILURE;

    libnet_clear_packet(pkt_d);

    return SUCCESS;
}

void
process_packets(struct pcap_pkthdr *pkthdr, u_int8_t *packet)
{
    u_int8_t pl[1500];
    u_int16_t n = 0; 
    struct libnet_ipv4_hdr *iphdr;
    struct libnet_ethernet_hdr *ehdr;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: process_packets()\n");
#endif

    memset(pl, 0, sizeof(pl));

    iphdr = (struct libnet_ipv4_hdr *)(packet + hdr_len);
    ehdr = (struct libnet_ethernet_hdr *)(packet);

    fprintf(stdout, "\n----------------------------------------------------------------------------\n");

    switch(iphdr->ip_p)
    {
        case IPPROTO_TCP: 
            n = tcp_hdr(packet, ntohs(iphdr->ip_len) - (iphdr->ip_hl * 4));
            break;

        case IPPROTO_UDP: 
            udp_hdr(packet, ntohs(iphdr->ip_len) - (iphdr->ip_hl * 4));
            break;

        case IPPROTO_ICMP: 
            icmpv4_hdr(packet, ntohs(iphdr->ip_len) - (iphdr->ip_hl * 4));
            break;
    }

    ipv4_hdr(iphdr, n);
    ethernet_hdr(ehdr);

    if(dump_pkt)
        dump_packet(packet + hdr_len, pkthdr->caplen - hdr_len);    

    return;
}

