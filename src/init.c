/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "init.h"

void
capture_loop(u_int8_t *user, struct pcap_pkthdr *pkthdr, u_int8_t *packet)
{
    if(display)
        process_packets(pkthdr, packet);

    if(!inject_packet())
        fatal_error("Unable to inject packet");

    return;
}

void
init()
{
    u_int8_t filter[255+1];
    u_int32_t data_link, localnet, netmask;
    pcap_dumper_t *p_dumper = NULL;

    struct bpf_program bpf;
    struct itimerval oval;
    struct libnet_ether_addr *hw_addr;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: init()\n");
#endif

    memset(&oval, 0, sizeof(struct itimerval));
    memset(&bpf, 0, sizeof(struct bpf_program));

    signal(SIGTERM, clean_exit);
    signal(SIGINT, clean_exit);
    signal(SIGQUIT, clean_exit);
    signal(SIGHUP, clean_exit);
    signal(SIGALRM, arp_cache_alrm);

    if((pkt_d = libnet_init(0, device, error_buf)) == NULL)
        fatal_error("Unable to initialize packet injection");

    if(device == NULL)
        if((device = pcap_lookupdev(error_buf)) == NULL)
            fatal_error("%s: Check device permissions", error_buf);

    if((pkt = pcap_open_live(device, 1500, 1, 500, error_buf)) == NULL)
        fatal_error("Unable to open device: %s", error_buf);

    if(strlen(w_file) > 0)
    {
#ifdef DEBUG
        fprintf(stdout, "DEBUG: Writing to capture file: %s\n", w_file);
#endif

        if((p_dumper = pcap_dump_open(pkt, w_file)) == NULL)
            fatal_error("Unable to initialize packet capture: %s", pcap_geterr(pkt));

        display--;
    }

    if(pcap_lookupnet(device, &localnet, &netmask, error_buf) < 0)
	fprintf(stderr, "\nWarning: Unable to lookup network: %s\n", error_buf);

    hw_addr = libnet_get_hwaddr(pkt_d);
    snprintf(filter, 255, "(ip src host %s and ether dst %0x:%0x:%0x:%0x:%0x:%0x) or (ip dst host %s and ether dst %0x:%0x:%0x:%0x:%0x:%0x)", 
        host_ip, 
        hw_addr->ether_addr_octet[0], 
        hw_addr->ether_addr_octet[1], 
        hw_addr->ether_addr_octet[2], 
        hw_addr->ether_addr_octet[3], 
        hw_addr->ether_addr_octet[4], 
        hw_addr->ether_addr_octet[5], 
        host_ip,
        hw_addr->ether_addr_octet[0],
        hw_addr->ether_addr_octet[1],
        hw_addr->ether_addr_octet[2],
        hw_addr->ether_addr_octet[3],
        hw_addr->ether_addr_octet[4],
        hw_addr->ether_addr_octet[5]);

    fprintf(stdout, "GW: %s\n", filter);

    if(pcap_compile(pkt, &bpf, filter, 0, netmask) < 0)
        fatal_error("Unable to compile packet filters: %s\n", pcap_geterr(pkt));

    if(pcap_setfilter(pkt, &bpf) < 0)
        fatal_error("Unable to set packet filters: %s", pcap_geterr(pkt));

#ifdef HAVE_FREECODE
    pcap_freecode(&bpf); 
#endif /* HAVE_FREECODE */

    if((data_link = pcap_datalink(pkt)) < 0)
        fatal_error("Unable to determine datalink type: %s", pcap_geterr(pkt));

    hdr_len = retrieve_datalink_hdr_len(data_link);

    fprintf(stdout, "4g8: Lets see what %s is up to. (Device: %s)\n", host_ip, device);

    oval.it_interval.tv_sec = 2;
    oval.it_value.tv_sec = 1;
    setitimer(ITIMER_REAL, &oval, 0);

    if(pcap_loop(pkt, 0, 
        (display == 1) ? (pcap_handler)capture_loop : (pcap_handler)pcap_dump, 
        (display == 1) ? NULL : (u_int8_t *)p_dumper) < 0)
    {
        fatal_error("Unable to initialize pcap_loop: %s", pcap_geterr(pkt));
    }

    clean_exit(SUCCESS);

    return;
}
