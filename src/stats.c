/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "stats.h"

void
injection_stats()
{
    fprintf(stdout, "\n");

    return;
}

void
capture_stats()
{
    struct pcap_stat p_stats;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: capture_stats()\n");
#endif

    memset(&p_stats, 0, sizeof(struct pcap_stat));

    pcap_stats(pkt, &p_stats);

    print_separator(0, 1, "4g8 Capture Statistics");

    fprintf(stdout, "Received: %u  Dropped: %u",
        p_stats.ps_recv, p_stats.ps_drop);

    fprintf(stdout, "\n");

    return;
}

