/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "udp_hdr.h"

void
udp_hdr(u_int8_t *packet, u_int16_t diff)
{
    struct libnet_udp_hdr *udphdr;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: udp_hdr()\n");
#endif

    udphdr = (struct libnet_udp_hdr *)(packet + IPV4_H + hdr_len);

    fprintf(stdout, "UDP header:  Src Port: %d  Dst Port: %d  Len: %d  ",
        htons(udphdr->uh_sport),
        htons(udphdr->uh_dport),
        ntohs(udphdr->uh_ulen)); 
    
    fprintf(stdout, "\n");

    payload_len = ntohs(udphdr->uh_ulen) - 8;

    if(libnet_build_udp(
        htons(udphdr->uh_sport),
        htons(udphdr->uh_dport),
        ntohs(udphdr->uh_ulen),
        udphdr->uh_sum,
        (payload_len == 0) ? NULL : packet + hdr_len + IPV4_H + 8,
        payload_len, 
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build UDP header: %s", libnet_geterror(pkt_d));
    }

    return;
}

