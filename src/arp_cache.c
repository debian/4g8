/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include <pthread.h>
#include "arp_cache.h"

pthread_mutex_t conn_mutex = PTHREAD_MUTEX_INITIALIZER;

libnet_t *
arp_cache_mgr(u_int8_t action, libnet_t *pkt_d)
{
    u_int32_t s_paddr, r_paddr;
    u_int8_t gw_neaddr[6], host_neaddr[6];
    u_int8_t r_neaddr[] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0}; 

    struct libnet_ether_addr *hw_addr; 

#ifdef DEBUG
    fprintf(stdout, "DEBUG: arp_cache_mgr()\n");
#endif

    hw_addr = malloc(sizeof(struct libnet_ether_addr *));
    memset(hw_addr, 0, sizeof(struct libnet_ether_addr *));

    if((r_paddr = libnet_name2addr4(pkt_d, "0.0.0.0", 0)) == -1)
        fatal_error("Invalid receiver protocol address");

 
    // Host

    if(action == 0)
    {
        if(format_ethernet_addr(gw_mac, hw_addr->ether_addr_octet) == 0)
            fatal_error("Invalid sender ethernet address: %s", gw_mac);

        if((s_paddr = libnet_name2addr4(pkt_d, gw_ip, 0)) == -1)
            fatal_error("Invalid receiver protocol address: %s", gw_ip);
    }
    else
    if(action == 1)
    {
        hw_addr = libnet_get_hwaddr(pkt_d); 
 
        if((s_paddr = libnet_name2addr4(pkt_d, gw_ip, 0)) == -1)
            fatal_error("Invalid receiver protocol address: %s", gw_ip);
    }

    if(libnet_build_arp(
        ARPHRD_ETHER,                    
        ETHERTYPE_IP,                          
        6,                                      
        4,                                      
        2,                     
        hw_addr->ether_addr_octet,                           
        (u_int8_t *)&s_paddr,                          
        r_neaddr, 
        (u_int8_t *)&r_paddr,                          
        NULL,                                   
        0,                                     
        pkt_d,                                      
        0) == -1)                              
    {
        fatal_error("Unable to build ARP header: %s", libnet_geterror(pkt_d)); 
    } 

    if(format_ethernet_addr(host_mac, host_neaddr) == 0)
        fatal_error("Invalid sender ethernet address");

    if(libnet_build_ethernet(
        host_neaddr,
        hw_addr->ether_addr_octet,
        ETHERTYPE_ARP,
        NULL,
        0,
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build ethernet header");
    }

    libnet_write(pkt_d);
    libnet_clear_packet(pkt_d);

    // GATEWAY

    if(action == 0)
    {
        if(format_ethernet_addr(host_mac, hw_addr->ether_addr_octet) == 0)
            fatal_error("Invalid sender ethernet address: %s", host_mac);

        if((s_paddr = libnet_name2addr4(pkt_d, host_ip, 0)) == -1)
            fatal_error("Invalid receiver protocol address: %s", host_ip);
    }
    else
    if(action == 1)
    {
        hw_addr = libnet_get_hwaddr(pkt_d);

        if((s_paddr = libnet_name2addr4(pkt_d, host_ip, 0)) == -1)
            fatal_error("Invalid receiver protocol address: %s", host_ip);
    }

    if(libnet_build_arp(
        ARPHRD_ETHER,
        ETHERTYPE_IP,
        6,
        4,
        2,
        hw_addr->ether_addr_octet,
        (u_int8_t *)&s_paddr,
        r_neaddr,
        (u_int8_t *)&r_paddr,
        NULL,
        0,
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build ARP header: %s", libnet_geterror(pkt_d));
    }

    if(format_ethernet_addr(gw_mac, gw_neaddr) == 0)
        fatal_error("Invalid sender ethernet address");

    if(libnet_build_ethernet(
        gw_neaddr,
        hw_addr->ether_addr_octet,
        ETHERTYPE_ARP,
        NULL,
        0,
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build ethernet header");
    }

    libnet_write(pkt_d);
    libnet_clear_packet(pkt_d);

    return pkt_d;
}

void
arp_cache_alrm(int sig)
{
#ifdef DEBUG
    fprintf(stdout, "DEBUG: arp_cache_alrm(%d)\n", sig);
#endif

//    pthread_mutex_lock(&conn_mutex);
    arp_cache_mgr(1, pkt_d);
//    pthread_mutex_unlock(&conn_mutex);

    return;
}

