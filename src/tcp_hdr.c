/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (C) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "tcp_hdr.h"

u_int16_t
tcp_hdr(u_int8_t *packet, u_int16_t diff)
{
    u_int8_t flags[7];

    struct libnet_tcp_hdr *tcphdr;
    struct servent *port_src, *port_dst;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: tcp_hdr()\n");
#endif

    port_src = malloc(sizeof(struct servent));
    port_dst = malloc(sizeof(struct servent));

    memset(port_src, 0, sizeof(struct servent));
    memset(port_dst, 0, sizeof(struct servent));
    memset(flags, 0, sizeof(flags));

    tcphdr = (struct libnet_tcp_hdr *)(packet + IPV4_H + hdr_len);

    if(tcphdr->th_flags & TH_URG)
        strcat(flags, "U");

    if(tcphdr->th_flags & TH_ACK)
        strcat(flags, "A");

    if(tcphdr->th_flags & TH_PUSH)
        strcat(flags, "P");

    if(tcphdr->th_flags & TH_RST)
        strcat(flags, "R");

    if(tcphdr->th_flags & TH_SYN)
        strcat(flags, "S");

    if(tcphdr->th_flags & TH_FIN)
        strcat(flags, "F");

    if(strlen(flags) == 0)
        strcpy(flags, "None");

    fprintf(stdout, "TCP header:  Src Port: %d  Dst Port: %d  Flag(s): %s\n",
        htons(tcphdr->th_sport),
        htons(tcphdr->th_dport),
        flags);

    fprintf(stdout, "\t     Window: %d  ", htons(tcphdr->th_win));

    if(tcphdr->th_seq > 0)
        fprintf(stdout, "Seqn: %lu  ", (u_long)ntohl(tcphdr->th_seq));

    if(tcphdr->th_ack > 0)
        fprintf(stdout, "Ackn: %lu  ", (u_long)ntohl(tcphdr->th_ack));

    if(tcphdr->th_urp)
        fprintf(stdout, "Urg: %d  ", ntohs(tcphdr->th_urp));

    fprintf(stdout, "\n");

    payload_len = diff - (tcphdr->th_off * 4);

    if(libnet_build_tcp(
        htons(tcphdr->th_sport),
        htons(tcphdr->th_dport),
        (u_long)ntohl(tcphdr->th_seq),
        (u_long)ntohl(tcphdr->th_ack),
        tcphdr->th_flags,
        htons(tcphdr->th_win),
        0,
        ntohs(tcphdr->th_urp),
        TCP_H + payload_len, 
        (payload_len == 0) ? NULL : packet + hdr_len + IPV4_H + (tcphdr->th_off * 4),
        payload_len, 
        pkt_d,
        0) == -1)
    {
        fatal_error("Unable to build TCP header: %s", libnet_geterror(pkt_d));
    }


    return (tcphdr->th_off * 4) - TCP_H;
}
