/*
 * author: Darren Bounds <dbounds@intrusense.com>
 * copyright: Copyright (c) 2002 by Darren Bounds
 * license: This software is under GPL version 2 of license
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * 4g8 official page at http://.net
 */

#include "main.h"

int
main(int argc, char *argv[])
{
    display = 1;
    dump_pkt = 0;
    opterr = 0;

#ifdef DEBUG
    fprintf(stdout, "DEBUG: main()\n");
#endif

    while((opt = getopt(argc, argv, "G:g:hi:S:s:vw:X")) != -1)
    {
        switch(opt)
        {
            case 'g':
                if(!(gw_ip = strdup(optarg)))
                    fatal_error("Memory unavailable for: %s", optarg);
             
                break;

            case 'G':
                if(!(gw_mac = strdup(optarg)))
                    fatal_error("Memory unavailable for: %s", optarg);

                break;

            case 'h':
                print_usage();
                exit(FAILURE);
                break;

            case 'i':
                if(!(device = strdup(optarg)))
                    fatal_error("Memory unavailable for: %s", optarg);

                break;

            case 's':
                if(!(host_ip = strdup(optarg)))
                    fatal_error("Memory unavailable for: %s", optarg);

                break;

            case 'S':
                if(!(host_mac = strdup(optarg)))
                    fatal_error("Memory unavailable for: %s", optarg);

                break;

            case 'v':
                print_version();
                exit(FAILURE);
                break;

            case 'w':
                strncpy(w_file, optarg, OPT_MAXLEN);
                break;

            case 'x': case 'X':
                dump_pkt = 1;
                break;
        }
    }

    if(!host_ip || !host_mac || !gw_ip || !gw_mac)
        print_usage();

    init(argv[optind], 0);

    exit(SUCCESS);
}        
