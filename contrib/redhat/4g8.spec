%define version 1.0 
Name:           4g8
Version:        %{version}
Release:        1
Source:         http://forgate.sourceforge.net/4g8-%{version}.tgz
URL:            http://forgate.sourceforge.net/
License:        GPL
Group:          Networking/Utilities
BuildRoot:      /var/tmp/%{name}-rpmroot
Summary:        Packet Diversion And Interception For Switched LANs 
Vendor:         Darren Bounds <dbounds@intrusense.com>
BuildRequires:  libnet >= 1.1
Requires:       libnet >= 1.1
%description

4G8 was written as a proof of concept in one method of capturing 
traffic flows from a 3rd party on a switched network. 4G8 uses ARP cache 
poisoning, packet capture and packet reconstruction to perform it's task. It 
should work with nearly all TCP, ICMP and UDP IPv4 traffic. 

%prep
%setup

%build
CC='gcc -I/usr/include/pcap' ./configure --prefix=/usr
make

%install
mkdir -p $RPM_BUILD_ROOT/usr
make prefix=$RPM_BUILD_ROOT/usr install

%changelog
* Tue Apr 13 2004 Darren Bounds <dbounds@intrusenes.com>
  Updated for 1.0 sources.
* Sun Jan 25 2004 Darren Bounds <dbounds@intrusenes.com>
  Updated in response to Fortinet Cease and Desist request. 
* Sun Jan 25 2004 Darren Bounds <dbounds@intrusenes.com>
  Updated for 0.9b.
* Fri Jan 23 2004 Darren Bounds <dbounds@intrusense.com>
  Initial wrap. 

%files
%defattr(-,root,root)
%attr(755,root,root)                    /usr/sbin/4g8
%doc                                    INSTALL LICENSE VERSION

%clean
rm -rf $RPM_BUILD_ROOT
